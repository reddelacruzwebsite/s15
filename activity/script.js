/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

console.log(`------ACTIVITY 1`);

	const firstName = "Red";
	const lastName = "Dela Cruz";
	console.log(`My full name is ${firstName} ${lastName}.`);

	const myAge = 32;
	console.log(`My current age is: ${myAge}.`);

	const hobbies = ["annoying my cats", " cooking", " watching movies", " and playing on Switch"];
	console.log(`My hobbies are: ${hobbies}.`);

	const workAddress = {
		houseNumber: "B30",
			street: "L17",
			city: "General Trias",
			state: "Cavite"
	}

	console.log(`I live at ${workAddress.houseNumber} ${workAddress.street} ${workAddress.city} ${workAddress.state}.`);



console.log(`------ACTIVITY 2`);

	let fullName = "Steve Rogers";
	console.log("My full name is" + name);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false

	}
	console.log("My Full Profile: ")
	console.log(profile);

	 fullName = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	let lastLocation = "Arctic Ocean";
	lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);










